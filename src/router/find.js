const Find = () => import("../views/find/Find");

export default [
  {
    path: "/find",
    name: "find",
    component: Find
  }
];
