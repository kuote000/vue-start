import Vue from "vue";
import Router from "vue-router";
Vue.use(Router);

import Home from "./home";
import Classify from "./classify";
import Find from "./find";
import My from "./my";

const router = new Router({
  mode: "history",
  routes: [
    ...Home,
    ...Classify,
    ...Find,
    ...My,
    {
      path: "*",
      redirect: "/home"
    }
  ]
});

export default router;
