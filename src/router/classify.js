const Classify = () => import("../views/classify/Classify");

export default [
  {
    path: "/classify",
    name: "classify",
    component: Classify
  }
];
