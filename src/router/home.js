const Home = () => import("../views/home/Home");

export default [
  {
    path: "/home",
    name: "home",
    component: Home
  }
];
