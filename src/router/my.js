const My = () => import("../views/my/My");

export default [
  {
    path: "/my",
    name: "my",
    component: My
  }
];
